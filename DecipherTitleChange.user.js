// ==UserScript==
// @name          Decipher Title Changer
// @namespace     https://gitlab.com/radovid/decipherTitlesScript
// @version       2.0
// @description   Userscript for changing webpage titles (tab names) for decipher surveys to include Mac/SN
// @downloadURL https://gitlab.com/radovid/decipherTitlesScript/-/raw/master/DecipherTitleChange.user.js
// @updateURL https://gitlab.com/radovid/decipherTitlesScript/-/raw/master/DecipherTitleChange.user.js
// @match *://*.decipherinc.com/*
// @match *://*.focusvision.com/*
// @icon https://www.forsta.com/wp-content/uploads/2021/11/cropped-Forsta_Symbol_RGB_Navy-180x180.png
// ==/UserScript==


// Set regexp for different servers
var tes = /^https?:\/\/tes\.decipherinc\.com\//;
var selfserve = /^https?:\/\/selfserve\.decipherinc\.com\//;
var nk = /^https?:\/\/nk\.decipherinc\.com\//;
var nflx = /^https?:\/\/nflx\.decipherinc\.com\//;
var walmart = /^https?:\/\/walmart\.decipherinc\.com\//;

var serverUrls = [tes, selfserve, nk, nflx, walmart]; // Servers regexes
var serverNames = [' - TES', ' - SelfServe', ' - NK', ' - NFLX', ' - Walmart']; // Servers names

var survPath = /selfserve\/(?:[a-z0-9]{3,4})\/(\w+)/; // Regex for survey directory name
//var prjTitle = document.getElementsByClassName("title-1")[0].innerText;


function setTitle() {
    var url = location.href; // Get current url
    var currTitle = document.title; // Get default title
    var title = '';
    var serverName = '';
    var surveyNum = '';
    var decServer = url.split('.')[0].split('/')[2] + ': '; // url.includes("twitterfeedback") ? 'twitter/' : ( url.includes("1f59") || url.includes("252c") ) && url.includes("selfserve") ? 'M3/' :

    const chkD = domainBit => url.includes(domainBit); // Function to check urls

    // Set appropriate name for portal page
    const conditions = [
        { condition: /error/i.test(currTitle), title: currTitle },
        { condition: chkD("/apps/report/") && chkD("/edit/"), title: "Edit Crosstab" },
        { condition: chkD("/apps/report/") && chkD("#!/report/"), title: "Crosstab Run" },
        { condition: chkD("/apps/report/"), title: "Crosstabs" },
        { condition: chkD("/apps/dashboard/") && chkD(":edit"), title: "Edit Dashboard" },
        { condition: chkD("/apps/dashboard/") && chkD(":view"), title: currTitle },
        { condition: chkD("/apps/dashboard/"), title: "Dashboards" },
        { condition: chkD("/apps/respondents/"), title: "View/Edit Responses" },
        { condition: chkD("/apps/takesurvey/"), title: "Test Survey" },
        { condition: chkD("?config=GmGrUA&run=1"), title: "Data Downloads" },
        { condition: chkD("?markers=1"), title: "Markers" },
        { condition: chkD(":vars"), title: "Variables" },
        { condition: chkD(":edit"), title: "Edit Data" },
        { condition: chkD("admin/sst/list"), title: "Test Data History" },
        { condition: chkD("admin/sst/sst"), title: "Run Test Data / check for errors" },
        { condition: chkD("admin/vc/list"), title: "Change History" },
        { condition: chkD("/apps/projectwarnings"), title: "Project Warnings" },
        { condition: chkD("/apps/campaign"), title: "Campaigns View" },
        { condition: chkD("/apps/themeeditor"), title: "Theme Editor" },
        { condition: chkD("/apps/mls"), title: "Lang Manager" },
        { condition: chkD("/apps/filemanager"), title: "File Manager" },
        { condition: chkD("/apps/distribution"), title: "Sample Sources" },
        { condition: chkD("admin/users/audit-survey"), title: "Audit Log" },
        { condition: chkD("/projects/detail"), title: "Project Details" },
    ];

    const matchedCondition = conditions.find(item => item.condition); // Find item matching in url
    title = matchedCondition ? matchedCondition.title : currTitle.split(' ')[0]; // Set corresponding title

    // Set decipher server name
    for (var i in serverUrls) {
        if (serverUrls[i].test(url)) {
            serverName = serverNames[i];
            break;
        }
    };

    // Set survey directory name
    if (survPath.test(url)) {
        surveyNum = url.match(survPath)[1];
        // Check if temp/trans/api
        if (/\/temp[-_]?/i.test(url)) {
            surveyNum = surveyNum + '/temp';
        } else if (/\/(?:trans|translation)/i.test(url)) {
            surveyNum = surveyNum + '/trans';
        } else if (/\/?API/i.test(url)) {
            surveyNum = surveyNum + '/API';
        }
    } else {
        let dirName = url.split('/').slice(-1)[0];
        surveyNum = url.split('/').slice(-2)[0] + '/' + dirName.match(/(\w+).*/)[1];
    }
    document.title = surveyNum + ': ' + title + serverName;

} // end setTitle function

// On some pages title is set with JS and timeout is needed so our set doesn't get overwritten
setTimeout( function() {
    setTitle();
}, 500);

// Sometimes page isn't reloaded when changed and setTitle won't run, hashchange fixes this
window.addEventListener('hashchange', function(){
    if (location.href.includes("#!")) {
        setTitle();
    }
});

