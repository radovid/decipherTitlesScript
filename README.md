# Decipher Title Changer

Userscript for changing webpage titles (tab names) for decipher surveys to include Mac/SN.

You need Greasemonkey or Tampermonkey browser extension installed first, then to install the userscript just click the url below:
https://gitlab.borsolutions.com/rvidenov/deciphertitlesscript/-/raw/master/DecipherTitleChange.user.js
